#!/bin/bash
#=============================================================================
#SBATCH --account=bb1018
#SBATCH --job-name=remap_nn.run
#SBATCH --partition=prepost
#####SBATCH --nodes=10
#####SBATCH --threads-per-core=2
######SBATCH --mem=120000
#SBATCH --time=12:00:00                 # The time the job will take to run.
#SBATCH --mail-type=END                 # Type of email notification- BEGIN,END,FAIL,ALL
#SBATCH --mail-user=behrooz.keshtgar@kit.edu
#SBATCH --output=/work/bb1135/b381185/output/LC1_Limited_channel/icon-v.2.6.2.2_2km/LC1-channel-4000x9000km-2km-0011/LOG.remap_nn.run.%j.o
#SBATCH --error=/work/bb1135/b381185/output/LC1_Limited_channel/icon-v.2.6.2.2_2km/LC1-channel-4000x9000km-2km-0011/LOG.remap_nn.run.%j.o
#SBATCH --exclusive
#=========================================================================================

# change the simulation names and directory accordingly

#mkdir LC1-channel-4000x9000km-2km-0011_remapped
cd LC1-channel-4000x9000km-2km-0011_remapped

for day in 01 02 03 04 05 06 07 08 09 10; do

for hour in 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 ; do

# nearest neighbour interpolation
cdo -P 48 remap,../grid_desc_2.5km,../weights_nn_2.5km.nc ../icon-atm3d_ML_202101${day}T${hour}0000Z.nc ./icon-atm_ML_reg_202101${day}T${hour}0000Z.nc

# conservative remapping
cdo -P 48 remapcon,../grid_desc_0.5x0.5 ./icon-atm_ML_reg_202101${day}T${hour}0000Z.nc ./icon-atm3d_ML_reg_con_0.5x0.5_202101${day}T${hour}0000Z.nc

cdo -P 48 remapcon,../grid_desc_1x1 ./icon-atm_ML_reg_202101${day}T${hour}0000Z.nc ./icon-atm3d_ML_reg_con_1x1_202101${day}T${hour}0000Z.nc

rm ./icon-atm_ML_reg_202101${day}T${hour}0000Z.nc


done

done



