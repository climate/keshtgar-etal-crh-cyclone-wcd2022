# Scripts for "Cloud-radiative impact on the dynamics and predictability of an idealized extratropical cyclone"

Keshtgar et al. (2023), https://doi.org/10.5194/wcd-2022-35

Code repository for the ICON simulation run scripts, scripts for deriving baroclinic life cycle initial conditions, postprocessing of model output files, and the analysis scripts.
The post-processed data used in the analysis along with a copy of the scripts here are also available at the LMU open data server (https://doi.org/10.57970/h1y02-bjv70).
