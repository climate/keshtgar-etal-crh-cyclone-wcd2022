import numpy as np
import netCDF4 as nc
import matplotlib.pyplot as plt

def load_levelinfo():
    from netCDF4 import Dataset
    file = Dataset('./ifs2icon_verticalgridinfo_137levels.nc', 'r')
    hyam = np.squeeze(np.array(file.variables['hyam']))
    hybm = np.squeeze(np.array(file.variables['hybm']))   
    hyai = np.squeeze(np.array(file.variables['hyai']))
    hybi = np.squeeze(np.array(file.variables['hybi']))   
    lev  = np.squeeze(np.array(file.variables['lev' ]))
    lev_2= np.squeeze(np.array(file.variables['lev_2']))    
    return hyam, hybm, hyai, hybi, lev, lev_2
    
hyam, hybm, hyai, hybi, lev, lev_2 = load_levelinfo()
    
# constants
That   = 1.0
phihat = 45.0
m      = 7.06

#latitude-longitude grid
lat  = np.linspace(-90,90,360)
lon  = np.linspace(0,360,720)
nlat = lat.size
nlon = lon.size
nz   = lev.size

# latitude in radians
latrad = lat*np.pi/180.0
lonrad = lon*np.pi/180.0

# Tp temperature perturbation
Tp = np.zeros((nz, nlat, nlon)) 
for j in range(0, nlat):
    for i in range(0,nlon):
        Tp[:, j, i] = That*np.sin(m*lonrad[i])*np.power(1/np.cosh(m*(latrad[j]-phihat*np.pi/180.0)),2) 

#---------------------------------------
# save to netcdf files
#---------------------------------------
ncfile = nc.Dataset('lc1_tperturb_720x360.nc', 'w', clobber=True, format='NETCDF3_CLASSIC')
ncfile.description = 'Temperature perturbation Tprime for the initial condition of lifecylce, Eq(10) of Polvani and Esler 2007 JGR'
 
# dimensions
ncfile.createDimension('lat', nlat)
ncfile.createDimension('lon', nlon)
ncfile.createDimension('lev', nz)
ncfile.createDimension('lev_2', 1)
ncfile.createDimension('nhym', nz)
ncfile.createDimension('nhyi', nz+1)
ncfile.createDimension('time', 1)

# variables
nc_latitude  = ncfile.createVariable('lat'     , 'f8', ('lat',))
nc_longitude = ncfile.createVariable('lon'     , 'f8', ('lon',))
nc_lev       = ncfile.createVariable('lev'     , 'f8', ('lev',))
nc_lev_2     = ncfile.createVariable('lev_2'   , 'f8', ('lev_2',))
nc_hyam      = ncfile.createVariable('hyam'    , 'f8', ('nhym',))
nc_hybm      = ncfile.createVariable('hybm'    , 'f8', ('nhym',))
nc_hyai      = ncfile.createVariable('hyai'    , 'f8', ('nhyi',))
nc_hybi      = ncfile.createVariable('hybi'    , 'f8', ('nhyi',))
nc_time      = ncfile.createVariable('time'    , 'f8', ('time',))
nc_Tp        = ncfile.createVariable('Tp'      , 'f4', ('time', 'lev', 'lat', 'lon'))
  
# set horizontal grid data
nc_latitude[:]  = lat
nc_longitude[:] = lon

# set vertical grid data
nc_lev[:]       = lev
nc_lev.standard_name = "hybrid_sigma_pressure"
nc_lev.long_name = "hybrid level at layer midpoints"
nc_lev.formula = "hyam hybm (mlev=hyam+hybm*aps)"
nc_lev.formula_terms = "ap: hyam b: hybm ps: aps"
nc_lev.units = "level"
nc_lev.positive = "down"
nc_lev_2[:]       = lev_2
nc_lev_2.standard_name = "hybrid_sigma_pressure"
nc_lev_2.long_name = "hybrid level at layer midpoints"
nc_lev_2.formula = "hyam hybm (mlev=hyam+hybm*aps)"
nc_lev_2.formula_terms = "ap: hyam b: hybm ps: aps"
nc_lev_2.units = "level"
nc_lev_2.positive = "down"
nc_hyam[:] = hyam
nc_hyam.long_name = "hybrid A coefficient at layer midpoints"
nc_hyam.units = "Pa"
nc_hybm[:] = hybm
nc_hybm.long_name = "hybrid B coefficient at layer midpoints"
nc_hybm.units = "1"
nc_hyai[:] = hyai
nc_hyai.long_name = "hybrid A coefficient at layer interfaces"
nc_hyai.units = "Pa"
nc_hybi[:] = hybi
nc_hybi.long_name = "hybrid B coefficient at layer interfaces"
nc_hybi.units = "1"

# set time data
nc_time[0] = 23790716.25 # this is the time from ifs2icon_0010_R02B04_aquaplanet.nc
nc_time.standard_name = "time"
nc_time.units = "day as %Y%m%d.%f"
nc_time.calendar = "proleptic_gregorian"
nc_time.axis = "T"

# set variable data
nc_Tp[:,:,:,:]  = Tp

# set variable attributes
nc_Tp.units = "K"; nc_Tp.standard_name = "temperature"; nc_Tp.long_name = "Atmospheric temperature"

ncfile.close()
