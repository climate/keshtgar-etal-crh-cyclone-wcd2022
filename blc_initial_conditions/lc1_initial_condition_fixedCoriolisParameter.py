import numpy as np
import scipy.integrate
import matplotlib.pyplot as plt
import netCDF4 as nc
from numba import jit

@jit
def Tintegrand(latrad, z, zT, U0, a, Omega):
    f  = 2*Omega*np.sin(np.deg2rad(45.0))
    F  = np.power(np.sin(np.pi*np.power(np.sin(latrad),2)),3)
    if latrad<0: F=0.0
    u1 = U0*F*(z/zT)*np.exp(-0.5*(np.power(z/zT,2)-1))
    du1dz = u1*(1/z-z/np.power(zT,2))
    return (a*f+2*u1*np.tan(latrad))*du1dz

def load_levelinfo():
    from netCDF4 import Dataset
    file = Dataset('./ifs2icon_verticalgridinfo_137levels.nc', 'r')
    hyam = np.squeeze(np.array(file.variables['hyam']))
    hybm = np.squeeze(np.array(file.variables['hybm']))   
    hyai = np.squeeze(np.array(file.variables['hyai']))
    hybi = np.squeeze(np.array(file.variables['hybi']))   
    lev  = np.squeeze(np.array(file.variables['lev' ]))
    lev_2= np.squeeze(np.array(file.variables['lev_2']))    
    return hyam, hybm, hyai, hybi, lev, lev_2
    
hyam, hybm, hyai, hybi, lev, lev_2 = load_levelinfo()
    
# constants
# physical constants are set to the values used in icon-nwp-2.0.15/src/shared/mo_physical_constants.f90
u0     = 45.0           # in m/s
zT     = 13.0e3         # in m
H      = 7.5e3          # in m
R      = 287.04         # dry gas constant in J/(kg K) (parameter rd in ICON)
a      = 6.371229e6     # average Earth radius in m (parameter earth_radius in ICON)
Omega  = 7.29212e-5     # angular velocity in 1/s (parameter earth_angular_velocity in ICON) 
t0     = 300            # in K
Gamma0 = -6.5e-3        # in K/m
alpha  = 10             # unitless
kappa  = 2.0/7.0        # unitless
g      = 9.80665        # av. gravitational acceleration in m/s2 (parameter grav in ICON)
p0     = 1.0e5          # globally-uniform surface pressure in Pa
# for relative humidity following Booth et al., 2013 Climate Dynamics
zTrh   = 12.0e3        
rh0    = 0.80           # relative humidity scaling factor from 0..1

#latitude-longitude grid
lat  = np.linspace(-90, 90, 360)
lon  = np.linspace(0.0,360,10)
nlat = lat.size
nlon = lon.size

# vertical grid: for computation of initial state we convert the ifs2icon hybrid levels
# to height levels assuming a globally-uniform surface pressure (defined above)
# and defining ehight according to Polvani and Elsner as z = H ln (p0/p)
p  = hyam + hybm*p0
z  = H*np.log(p0/p)  # np.log is natural logarithm
nz   = z.size
#print(z, nz)

# latitude in radians
latrad = lat * np.pi/180.0 #lat2

# lifecycle 1
u1 = np.zeros((nz,nlat))+np.nan
F  = np.power(np.sin(np.pi*np.power(np.sin(latrad),2)),3); F[lat<0] = 0.0
for i in range(0, nz):
    for j in range(0, nlat):
        u1[i,j] = u0*F[j]*(z[i]/zT)*np.exp(-0.5*(np.power(z[i]/zT,2)-1))
#du1dz = u1*np.expand_dims(1/z-z/np.power(zT,2),axis=1)

# compute temperature profile in zonal wind balance with u1
t=np.zeros((nz, nlat)) + np.nan

# latitude independe reference profile
tr = np.zeros((nz, nlat)) + np.nan
for i in range(0, nz):
    tr[i, :] = t0 + Gamma0/np.power((np.power(zT,-alpha)+np.power(z[i],-alpha)),1/alpha)

# latitude dependent modification
tmp = np.zeros((nz, nlat)) + np.nan
for i in range(0, nz):
    for j in range(0, nlat):
        tmp[i, j] =scipy.integrate.quad(Tintegrand, 0, latrad[j], args=(z[i], zT, u0, a, Omega))[0]
t=tr-H/R*tmp
       
# potential temperature
theta=t*np.expand_dims(np.exp(kappa*z/H),axis=1)
   
# relative humidity
# follows Booth et al. 2013, Climate Dynamics 
rh = np.zeros((nz, nlat)) + np.nan
for i in range(0, nz):
    rh[i, :] = rh0*np.power(1-0.85*z[i]/zTrh, 1.25)
    if z[i]>14e3:
        rh[i, :] = 0.0
        

# specific humidty
# follows calculation in icon 
# /icon-nwp-2.0.15/src/atm_phy_schemes/mo_satad.f90 -> sat_pres_water,spec_humi
b1 = 610.78  # --> c1es in mo_convect_tables.f90 
b2w= 17.269  # --> c3les 
b3 = 273.15  # --> tmelt; melting temperature in K
b4w= 35.86   # --> c4les
sat_pres_water = b1*np.exp(b2w*(t-b3)/(t-b4w))
Rdv = 287.04/461.51   # Rd/Rv
o_m_Rdv = 1-Rdv       # 1-Rd/Rv
print(Rdv, o_m_Rdv)  
qv = np.zeros((nz, nlat))
for i in range(0, nz):
    for j in range(0, nlat):
        qv[i, j] = rh[i,j]*Rdv*sat_pres_water[i,j]/(p[i]-o_m_Rdv*sat_pres_water[i,j])

#---------------------------------------
# save to netcdf files
#---------------------------------------
ncfile = nc.Dataset('lc1_0.8rh0_initialcondition_r10x360.nc', 'w', clobber=True, format='NETCDF3_CLASSIC')
ncfile.description = 'Initial condition for LC1, for U and T according to Polvani and Esler 2007 JGR'
 
# dimensions
ncfile.createDimension('lat', nlat)
ncfile.createDimension('lon', nlon)
ncfile.createDimension('lev', nz)
ncfile.createDimension('lev_2', 1)
ncfile.createDimension('nhym', nz)
ncfile.createDimension('nhyi', nz+1)
ncfile.createDimension('time', 1)

# variables
nc_latitude  = ncfile.createVariable('lat'     , 'f8', ('lat',))
nc_longitude = ncfile.createVariable('lon'     , 'f8', ('lon',))
nc_lev       = ncfile.createVariable('lev'     , 'f8', ('lev',))
nc_lev_2     = ncfile.createVariable('lev_2'   , 'f8', ('lev_2',))
nc_hyam      = ncfile.createVariable('hyam'    , 'f8', ('nhym',))
nc_hybm      = ncfile.createVariable('hybm'    , 'f8', ('nhym',))
nc_hyai      = ncfile.createVariable('hyai'    , 'f8', ('nhyi',))
nc_hybi      = ncfile.createVariable('hybi'    , 'f8', ('nhyi',))
nc_time      = ncfile.createVariable('time'    , 'f8', ('time',))
nc_T         = ncfile.createVariable('T'       , 'f4', ('time', 'lev', 'lat', 'lon'))
nc_U         = ncfile.createVariable('U'       , 'f4', ('time', 'lev', 'lat', 'lon'))
nc_V         = ncfile.createVariable('V'       , 'f4', ('time', 'lev', 'lat', 'lon'))
nc_W         = ncfile.createVariable('W'       , 'f4', ('time', 'lev', 'lat', 'lon'))
nc_QV        = ncfile.createVariable('QV'      , 'f4', ('time', 'lev', 'lat', 'lon'))
nc_QC        = ncfile.createVariable('QC'      , 'f4', ('time', 'lev', 'lat', 'lon'))
nc_QI        = ncfile.createVariable('QI'      , 'f4', ('time', 'lev', 'lat', 'lon'))
nc_SST       = ncfile.createVariable('SST'     , 'f4', ('time', 'lat', 'lon'))   
nc_LNPS      = ncfile.createVariable('LNPS'    , 'f4', ('time', 'lev_2', 'lat', 'lon'))
nc_GEOP_SFC  = ncfile.createVariable('GEOP_SFC', 'f4', ('time', 'lev_2', 'lat', 'lon'))
nc_GEOP_ML   = ncfile.createVariable('GEOP_ML' , 'f4', ('time', 'lev_2', 'lat', 'lon'))
  
# set horizontal grid data
nc_latitude[:]  = lat
nc_longitude[:] = lon

# set vertical grid data
nc_lev[:]       = lev
nc_lev.standard_name = "hybrid_sigma_pressure"
nc_lev.long_name = "hybrid level at layer midpoints"
nc_lev.formula = "hyam hybm (mlev=hyam+hybm*aps)"
nc_lev.formula_terms = "ap: hyam b: hybm ps: aps"
nc_lev.units = "level"
nc_lev.positive = "down"
nc_lev_2[:]       = lev_2
nc_lev_2.standard_name = "hybrid_sigma_pressure"
nc_lev_2.long_name = "hybrid level at layer midpoints"
nc_lev_2.formula = "hyam hybm (mlev=hyam+hybm*aps)"
nc_lev_2.formula_terms = "ap: hyam b: hybm ps: aps"
nc_lev_2.units = "level"
nc_lev_2.positive = "down"
nc_hyam[:] = hyam
nc_hyam.long_name = "hybrid A coefficient at layer midpoints"
nc_hyam.units = "Pa"
nc_hybm[:] = hybm
nc_hybm.long_name = "hybrid B coefficient at layer midpoints"
nc_hybm.units = "1"
nc_hyai[:] = hyai
nc_hyai.long_name = "hybrid A coefficient at layer interfaces"
nc_hyai.units = "Pa"
nc_hybi[:] = hybi
nc_hybi.long_name = "hybrid B coefficient at layer interfaces"
nc_hybi.units = "1"

# set time data
nc_time[0] = 23790716.25 # this is the time from ifs2icon_0010_R02B04_aquaplanet.nc
nc_time.standard_name = "time"
nc_time.units = "day as %Y%m%d.%f"
nc_time.calendar = "proleptic_gregorian"
nc_time.axis = "T"

# set variable data
for i in range(0, nlon):
    nc_T[:,:,:,i]  = t
    nc_U[:,:,:,i]  = u1
    nc_V[:,:,:,i]  = 0.0
    nc_W[:,:,:,i]  = 0.0
    nc_QV[:,:,:,i] = qv
    nc_QC[:,:,:,i] = 0.0
    nc_QI[:,:,:,i] = 0.0
    nc_SST[:,:,i]         = t[nz-1,:] - 0.5 # as in Booth et al., 2013, Clim. Dynamics we set the SST to lowest-level initial T-0.5K
    nc_LNPS[:,:,:,i]      = np.log(p0)
    nc_GEOP_SFC[:,:,:,i]  = 0.0
    nc_GEOP_ML[:,:,:,i]   = g*z[nz-1]

# set variable attributes
nc_T.units = "K"; nc_T.standard_name = "temperature"; nc_T.long_name = "Atmospheric temperature"
nc_U.units = "m s**-1"; nc_U.standard_name = "u-wind"; nc_U.long_name = "Zonal wind"
nc_V.units = "m s**-1"; nc_V.standard_name = "v-wind"; nc_V.long_name = "Meridional wind"
nc_W.units = "Pa s**-1"; nc_W.standard_name = "lagrangian_tendency_of_air_pressure"; nc_W.long_name = "Vertical Velocity (Pressure) (omega=dp/dt)"
nc_QV.units = "kg kg**-1"; nc_QV.standard_name = "spec. humidity"; nc_QV.long_name = "Specific humidity"
nc_QC.units = "kg kg**-1"; nc_QC.standard_name = "cloud liq. water"; nc_QC.long_name = "Specific cloud liquid water content"
nc_QI.units = "kg kg**-1"; nc_QI.standard_name = "cloud ice"; nc_QI.long_name = "Specific cloud ice water content"
nc_GEOP_ML.units = "m**2 s**-2"; nc_GEOP_ML.standard_name = "geopotential"; nc_GEOP_ML.long_name = "Geopotential"
nc_SST.units = "K"; nc_SST.standard_name = "sst"; nc_SST.long_name = "Sea-surface temperature"
nc_LNPS.units = "n/a"; nc_LNPS.standard_name = "ln sfc pressure"; nc_LNPS.long_name = "Logarithm of surface pressure"
nc_GEOP_SFC.units = "m**2 s**-2"; nc_GEOP_SFC.standard_name = "sfc geopotential"; nc_GEOP_SFC.long_name = "Sfc Geopotential"
 
ncfile.close()
