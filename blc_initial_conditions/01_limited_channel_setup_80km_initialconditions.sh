#!/bin/bash
#=============================================================================
#SBATCH --account=bb1135
#SBATCH --job-name=remap_initial_conditions
#SBATCH --partition=compute
#SBATCH --nodes=2
#SBATCH --threads-per-core=2
#SBATCH --mem=120000
#SBATCH --output=output=/work/bb1135/b381185/simulation_setup/LC1_Limited_channel/LOG.initial_conditions.run.%j.o
#SBATCH --error=output=/work/bb1135/b381185/simulation_setup/LC1_Limited_channel/LOG.initial_conditions.run.%j.o
#SBATCH --exclusive
#SBATCH --time=01:00:00
#=========================================================================================
# This script prepares the initial files for a baroclinic life cycle simulation using
# DWD ICONTOOLS.
#=========================================================================================
#-----------------------------------------------------------------------------------------
# Introduce switches to decide what the script will do.
# Switches can be set to "yes" or "no".

# create new folder "inputs_planar_channel_51x81_2km"
makefolder=yes

# run python scripts to generate initial files for LC1
ini_python_ctl=yes

# run python script to generate the temperature perturbation
tpert_python=yes

# remap initial files from python scripts to channel grid
remap_ini_python_lc1=yes

# remap initial data from IFS to channel grid
remap_ifs=yes

# merge remapped initial files
merge_python_ifs_lc1=yes

#-----------------------------------------------------------------------------------------
# Load modules 

module purge
module load anaconda3/bleeding_edge
module load nco/4.7.5-gcc64
module load ncl/6.5.0-gccsys

#--------------------------------------------------------------------------------------
# ICONTOOLS directory

ICONTOOLS_DIR=/pf/b/b381185/ICON/icon_tutorial/dwd_icon_tools/icontools

BINARY_ICONSUB=iconsub_mpi
BINARY_REMAP=iconremap_mpi
BINARY_GRIDGEN=icongridgen

#--------------------------------------------------------------------------------------
# file with channel grid

gridfile=Channel_4000x9000_80000m_with_boundary.nc

#--------------------------------------------------------------------------------------
# make folder in which the interpolated data will be stored
if [[ "$makefolder" == "yes" ]]; then
    mkdir planar_channel_51x81_80km
fi

#--------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------

cd inputs

# 1- Generate initial conditions for baroclinic life cycle 1

# 1.1 initial conditions
if [[ "$ini_python_lc1" == "yes" ]]; then
    echo "#####################################################"
    echo "Generate initial conditions (lc1)"
    echo "#####################################################"
    # run python script
    python ../lc1_initial_condition_fixedCoriolisParameter.py
    # extend initial data to 720x360 grid
    cdo remapbil,r720x360 -setgrid,r10x360 lc1_0.8rh0_initialcondition_r10x360.nc lc1_0.8rh0_initialcondition_r720x360.nc
    # remove output from python script
    rm lc1_0.8rh0_initialcondition_r10x360.nc
fi

# 1.2 temperature perturbation
if [[ "$tpert_python" == "yes" ]]; then
    echo ""
    echo "#####################################################"
    echo "Generate temperature perturbation"
    echo "#####################################################"
    python ../lc1_perturbation_condition_wavenumber7.py
fi

# 1.3 add temperature perturbation to temperature field
if [[ "$ini_python_lc1" == "yes" ]]; then
    echo ""
    echo "#####################################################"
    echo "Merge initial file and temperature perturbation (lc1)"
    echo "#####################################################"
    cdo merge -delvar,T lc1_0.8rh0_initialcondition_r720x360.nc -add -selvar,T lc1_0.8rh0_initialcondition_r720x360.nc lc1_tperturb_720x360.nc lc1_0.8rh0_r720x360.nc
    # remove temporary data
    rm lc1_0.8rh0_initialcondition_r720x360.nc
    rm lc1_tperturb_720x360.nc
fi

#--------------------------------------------------------------------------------------
# 2- Remap the lc1 initial file onto the channel grid using DWD ICONTOOLS

cd ../planar_channel_51x81_2km

# NOTE: do not use indent for "EOF"; otherwise the calculation terminates with an error
if [[ "$remap_ini_python_lc1" == "yes" ]]; then
    echo ""
    echo "#####################################################"
    echo "Remap initial file (lc1) onto channel grid"
    echo "#####################################################"
    
    for field in  U V W QV QC QI SST LNPS GEOP_SFC GEOP_ML T ; do
    
    cat >> NAMELIST_ICONREMAP_FIELDS << EOF
    !
    &input_field_nml
     inputname      = "${field}"
     outputname     = "${field}"
     intp_method    = 3
    /
EOF

    done

    #cat NAMELIST_ICONREMAP_FIELDS

    cat > NAMELIST_ICONREMAP << EOF
    &remap_nml
     in_grid_filename  = ''
     in_filename       = '../inputs/lc1_0.8rh0_r720x360.nc'
     in_type           = 1
     out_grid_filename = '../inputs/${gridfile}'
     out_filename      = 'lc1_rh80.nc'
     out_type          = 2
     out_filetype      = 4
     l_have3dbuffer    = .false.
     ncstorage_file    = "ncstorage.tmp"
    /
EOF

    # run DWD ICONTOOLS
    ${ICONTOOLS_DIR}/${BINARY_REMAP} \
                --remap_nml NAMELIST_ICONREMAP                                  \
                --input_field_nml NAMELIST_ICONREMAP_FIELDS 2>&1
    
    # clean-up
    rm -f ncstorage.tmp*
    rm -f nml.log  NAMELIST_SUB NAMELIST_ICONREMAP NAMELIST_ICONREMAP_FIELDS
    rm -f ../inputs/lc1_0.8rh0_r720x360.nc
fi

#----------------------------------------------------------------------------
# 3- Remap the initial data from IFS for a zonally-symmetric aquaplanet to
#    the channel grid.

if [[ "$remap_ifs" == "yes" ]]; then
    echo ""
    echo "#####################################################"
    echo "Remap ifs initial file onto channel grid"
    echo "#####################################################"
    
    for field in T_SNOW W_SNOW RHO_SNOW ALB_SNOW SKT STL1 STL2 STL3 STL4 CI W_I Z0 LSM SMIL1 SMIL2 SMIL3 SMIL4 ; do
    
    cat >> NAMELIST_ICONREMAP_FIELDS << EOF
    !
    &input_field_nml
     inputname      = "${field}"
     outputname     = "${field}"
     intp_method    = 3
    /
EOF

    done

    cat > NAMELIST_ICONREMAP << EOF
    &remap_nml
     in_grid_filename  = '../inputs/icon_grid_0002_R02B06_G.nc'
     in_filename       = '../inputs/ifs2icon_0002_R02B06_aquaplanet.nc'
     in_type           = 2
     out_grid_filename = '../inputs/${gridfile}'
     out_filename      = 'ifs2icon_remapped.nc'
     out_type          = 2
     out_filetype      = 4
     l_have3dbuffer    = .false.
     ncstorage_file    = "ncstorage.tmp"
    /
EOF

    # run DWD ICONTOOLS
    ${ICONTOOLS_DIR}/${BINARY_REMAP} \
                --remap_nml NAMELIST_ICONREMAP                                  \
                --input_field_nml NAMELIST_ICONREMAP_FIELDS 2>&1
    
    # clean-up
    rm -f ncstorage.tmp*
    rm -f nml.log  NAMELIST_SUB NAMELIST_ICONREMAP NAMELIST_ICONREMAP_FIELDS
fi

#----------------------------------------------------------------------------
# 4- Construct the complete initial file by adding the remapped output from 
#    the python script and the remapped IFS data.

echo ""
echo "#####################################################"

if [[ "$merge_python_ifs_lc1" == "yes" ]]; then
    echo "Merge remapped initial files (LC1)"

    ncks -v T_SNOW,W_SNOW,RHO_SNOW,ALB_SNOW,SKT,STL1,STL2,STL3,STL4,CI,W_I,Z0,LSM,SMIL1,SMIL2,SMIL3,SMIL4 ifs2icon_remapped.nc temp1.nc

    ncks -A lc1_rh80.nc temp1.nc

    mv temp1.nc lc1_rh80_remapped.nc
fi

#----------------------------------------------------------------------------
# 5- Clean-up: remove temporary files
if [[ "$remap_ini_python_lc1" == "yes" ]]; then
    rm lc1_rh80.nc
fi

if [[ "$remap_ifs" == "yes" ]]; then
    rm ifs2icon_remapped.nc
fi
#---------------------------------------------------------------------------
