**ICON simulations**

LC1-channel-4000x9000km-2km-0002 : Simulation with no radiation

LC1-channel-4000x9000km-2km-0003 : Simulation with only cloud radiation

LC1-channel-4000x9000km-2km-0004 : Simulation with only cloud radiation scaled by a factor of 2

LC1-channel-4000x9000km-2km-0005 : Simulation with only cloud radiation until day 3

LC1-channel-4000x9000km-2km-0006 : Simulation with only cloud radiation until day 4

LC1-channel-4000x9000km-2km-0007 : Simulation with only cloud radiation until day 5

LC1-channel-4000x9000km-2km-0008 : Simulation with only cloud radiation until day 6

LC1-channel-4000x9000km-2km-0009 : Simulation with no radiation, and with 2-moment microphysics

LC1-channel-4000x9000km-2km-0010 : Simulation with only cloud radiation, and with 2-moment microphysics

LC1-channel-4000x9000km-80km-0001 : Simulation with all-sky radiation

LC1-channel-4000x9000km-80km-0002 : Simulation with no radiation

LC1-channel-4000x9000km-80km-0003 : Simulation with only cloud radiation
