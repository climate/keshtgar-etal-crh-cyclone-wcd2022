#! /bin/ksh
#=============================================================================

# mistral cpu batch job parameters

#SBATCH --account=bb1135
#SBATCH --job-name=LC12km0010
#SBATCH --partition=compute
#SBATCH --chdir=/pf/b/b381185/experiments/LC1-channel-4000x9000km-2km-0010
#SBATCH --nodes=95
#SBATCH --threads-per-core=2
# the following is needed to work around a bug that otherwise leads to
# a too low number of ranks when using compute,compute2 as queue
#SBATCH --mem=0
#SBATCH --output=/pf/b/b381185/experiments/LC1-channel-4000x9000km-2km-0010/LOG.exp.LC1-channel-4000x9000km-2km-0010.run.%j.o
#SBATCH --error=/pf/b/b381185/experiments/LC1-channel-4000x9000km-2km-0010/LOG.exp.LC1-channel-4000x9000km-2km-0010.run.%j.o
#SBATCH --exclusive
#SBATCH --time=08:00:00

#=============================================================================
set -x
ulimit -s unlimited
#=============================================================================
# OpenMP environment variables
# ----------------------------
export OMP_NUM_THREADS=$((${SLURM_JOB_CPUS_PER_NODE%%\(*} / 6 / 2))
export ICON_THREADS=$((${SLURM_JOB_CPUS_PER_NODE%%\(*} / 6 / 2))
export OMP_SCHEDULE=dynamic,1
export OMP_DYNAMIC="false"
export OMP_STACKSIZE=200M
#
# MPI variables
# -------------
no_of_nodes=${SLURM_JOB_NUM_NODES:=1}
mpi_procs_pernode=$((${SLURM_JOB_CPUS_PER_NODE%%\(*} / 2 / OMP_NUM_THREADS))
((mpi_total_procs=no_of_nodes * mpi_procs_pernode))

#=============================================================================

# load local setting, if existing
# -------------------------------
if [ -a ../setting ]
then
  echo "Load Setting"
  . ../setting
fi

# environment variables for the experiment and the target system
# --------------------------------------------------------------

export KMP_AFFINITY="verbose,granularity=core,compact,1,1"
export KMP_LIBRARY="turnaround"
export KMP_KMP_SETTINGS="1"
export OMP_WAIT_POLICY="active"
export OMPI_MCA_pml="cm"
export OMPI_MCA_mtl="mxm"
export OMPI_MCA_coll="^fca"
export MXM_RDMA_PORTS="mlx5_0:1"
export HCOLL_MAIN_IB="mlx5_0:1"
export HCOLL_ML_DISABLE_BARRIER="1"
export HCOLL_ML_DISABLE_IBARRIER="1"
export HCOLL_ML_DISABLE_BCAST="1"
export HCOLL_ENABLE_MCAST_ALL="1"
export HCOLL_ENABLE_MCAST="1"
export OMPI_MCA_coll_sync_barrier_after_alltoallv="1"
export OMPI_MCA_coll_sync_barrier_after_alltoallw="1"
export MXM_HANDLE_ERRORS="bt"
export UCX_HANDLE_ERRORS="bt"
export MALLOC_TRIM_THRESHOLD_="-1"
export KMP_AFFINITY="verbose,granularity=core,compact,1,1"
export KMP_LIBRARY="turnaround"
export KMP_KMP_SETTINGS="1"
export OMP_WAIT_POLICY="active"
export OMPI_MCA_pml="cm"
export OMPI_MCA_mtl="mxm"
export OMPI_MCA_coll="^fca"
export MXM_RDMA_PORTS="mlx5_0:1"
export HCOLL_MAIN_IB="mlx5_0:1"
export HCOLL_ML_DISABLE_BARRIER="1"
export HCOLL_ML_DISABLE_IBARRIER="1"
export HCOLL_ML_DISABLE_BCAST="0"
export HCOLL_ENABLE_MCAST_ALL="1"
export HCOLL_ENABLE_MCAST="1"
export OMPI_MCA_coll_sync_barrier_after_alltoallv="1"
export OMPI_MCA_coll_sync_barrier_after_alltoallw="1"
export MXM_HANDLE_ERRORS="bt"
export UCX_HANDLE_ERRORS="bt"
# load profile
# ------------
if [[ -a  /etc/profile ]]
then
        . /etc/profile
fi

#=============================================================================

# directories with absolute paths
# -------------------------------
basedir="/pf/b/b381185/ICON/icon-on-jet"
export basedir

# how to start the icon model
export START="srun --cpu-freq=HighM1 --kill-on-bad-exit=1 --nodes=${SLURM_JOB_NUM_NODES:-1} --cpu_bind=verbose,cores --distribution=block:block --ntasks=$((no_of_nodes * mpi_procs_pernode)) --ntasks-per-node=${mpi_procs_pernode} --cpus-per-task=$((2 * OMP_NUM_THREADS)) --propagate=STACK,CORE"

export MODEL="${basedir}/bin/icon"

# how to submit the next job
# --------------------------
submit="sbatch"
job_name="exp.LC1-channel-4000x9000km-2km-0010.run"

#=============================================================================

ulimit -s 2097152
ulimit -c 0

#-----------------------------------------------------------------------------
export EXPNAME="LC1-channel-4000x9000km-2km-0010"

# atmo namelist filename
atmo_namelist=NAMELIST_${EXPNAME}_atm

# directories definition
RUNSCRIPTDIR=/pf/b/b381185/experiments/LC1-channel-4000x9000km-2km-0010/  # run script directory

#-----------------------------------------------------------------------------
# global timing
start_date="2021-01-01T00:00:00Z"
end_date="2021-01-11T00:00:00Z" 

# restart intervals
checkpoint_interval="P1D"
restart_interval="P5D"

# output intervals
output_interval="PT1H"
file_interval="PT1H"

# regular  grid: nlat=96, nlon=192, npts=18432, dlat=1.875 deg, dlon=1.875 deg
reg_lat_def_reg=-90.0,1.0,90.0
reg_lon_def_reg=-180.0,1.0,180.0

#-----------------------------------------------------------------------------
# model parameters
model_equations=3             # equation system
#                     1=hydrost. atm. T
#                     1=hydrost. atm. theta dp
#                     3=non-hydrost. atm.,
#                     0=shallow water model
#                    -1=hydrost. ocean
#-----------------------------------------------------------------------------

# experiment directory, with plenty of space, create if new

EXPDIR=/work/bb1135/b381185/output/LC1_Limited_channel/icon-v.2.6.2.2_2km/${EXPNAME} 
if [ ! -d ${EXPDIR} ] ;  then
  mkdir -p ${EXPDIR}
fi
#
ls -ld ${EXPDIR}
if [ ! -d ${EXPDIR} ] ;  then
    mkdir ${EXPDIR}
fi
ls -ld ${EXPDIR}
check_error $? "${EXPDIR} does not exist?"

cd ${EXPDIR}

#-----------------------------------------------------------------------------
# Link experiment initial files
inputdir='/work/bb1135/b381185/simulation_setup/LC1_Limited_channel/planar_channel_51x81_2km'
# Grid
ln -sf $inputdir/Channel_4000x9000_2500m_with_boundary.nc grid_DOM01.nc
# Extpar
ln -sf $inputdir/extpar_remapped_12_months.nc extpar_DOM01.nc
# Initial file
ln -sf $inputdir/lc1_rh80_remapped.nc ifs2icon_init.nc
# Ozon
ln -sf $inputdir/ape_O3_remapped.nc o3_icon_DOM01.nc

# Model required files
ln -sf $basedir/data/rrtmg_lw.nc              ./
ln -sf $basedir/data/rrtmg_sw.nc              ./
ln -sf $basedir/data/ECHAM6_CldOptProps.nc    ./

#=============================================================================

# create ICON namelist parameters

cat > ${atmo_namelist} << EOF
&parallel_nml
 nproma         = 16  ! optimal setting 8 for CRAY; use 16 or 24 for IBM
 p_test_run     = .false.
 l_test_openmp  = .false.
 l_log_checks   = .false.
 num_io_procs   = 10   ! asynchronous output for values >= 1
 itype_comm     = 1
 iorder_sendrecv = 3  ! best value for CRAY (slightly faster than option 1)
 num_prefetch_proc = 1
/
&grid_nml
 dynamics_grid_filename  = 'grid_DOM01.nc'
 dynamics_parent_grid_id = 0
 lredgrid_phys           = .false.
 is_plane_cylinder       = .TRUE.
 l_limited_area          = .true.
 corio_lat               = 45.0
/
&initicon_nml
 init_mode   = 2           ! operation mode 2: IFS
 ifs2icon_filename = 'ifs2icon_init.nc' 
 zpbl1       = 500. 
 zpbl2       = 1000.
 l_sst_in    = .true. 
/
&limarea_nml
 itype_latbc    = 0            ! fix
/
&run_nml
 num_lev        = 75           ! 90
 dtime          = 24  ! 360 for R2B5 180 for R2B7    
 ldynamics      = .TRUE.
 ltransport     = .true.
 iforcing       = 3            ! NWP forcing
 ltestcase      = .false.      ! false: run with real data
 msg_level      = 7            ! print maximum wind speeds every 5 time steps
 ltimer         = .false.      ! set .TRUE. for timer output
 timers_level   = 1            ! can be increased up to 10 for detailed timer output
 output         = "nml"
/
&nwp_phy_nml
 inwp_gscp          = 4
 inwp_convection    = 1
 lshallowconv_only  = .true. ! use only shallow convection scheme
 inwp_radiation     = 1
 inwp_cldcover      = 1
 inwp_turb          = 1
 inwp_satad         = 1
 inwp_sso           = 1
 inwp_gwd           = 1
 inwp_surface       = 1
 icapdcycl          = 3
 latm_above_top  = .false.  ! the second entry refers to the nested domain (if present)
 efdt_min_raylfric = 7200.
 ldetrain_conv_prec = .false.
 itype_z0         = 2
 icpl_aero_conv   = 0 
 icpl_aero_gscp   = 0
 ! resolution-dependent settings - please choose the appropriate one
 dt_rad    = 720 
 dt_conv   = 300 
 dt_sso    = 300 
 dt_gwd    = 300. 
 lcloudradonly    = .true. ! no radiation temperature tendency contribution i.e no radiation simulation
 ldiag_ddt_temp_dyn2          = .true.
/
&turbdiff_nml
 tkhmin  = 0.75  
 tkmmin  = 0.75             
 pat_len = 100. !750.
 c_diff  = 0.2
 rat_sea = 10  
 ltkesso = .true.
 frcsmot = 0.2      
 imode_frcsmot = 2  
 itype_sher = 3    
 ltkeshs    = .true.
 a_hshr     = 2.0     
/
&lnd_nml
 ntiles         = 3      !!! 1 for assimilation cycle and forecast
 nlev_snow      = 3      !!! 1 for assimilation cycle and forecast
 lmulti_snow    = .true. !!! .false. for assimilation cycle and forecast
 itype_heatcond = 2
 idiag_snowfrac = 2
 lsnowtile      = .false.  !! later on .true. if GRIB encoding issues are solved
 lseaice        = .true. !!Sophia Schäfer, 23/03/2017: reads in sea ice (e.g. zero ice), instead of setting by temperature
 llake          = .false.
 itype_lndtbl   = 3  ! minimizes moist/cold bias in lower tropical troposphere
 itype_root     = 2  			        
/
&radiation_nml
 irad_o3       = 4
 irad_aero     = 0
 irad_cfc11    = 0
 irad_cfc12    = 0
 albedo_type   = 2 ! Modis albedo
 vmr_co2       = 348.0e-6 ! values representative for 2012
 vmr_ch4       = 1650.0e-09
 vmr_n2o       = 396.0e-09
 vmr_o2        = 0.20946
 izenith       = 3
 vmr_cfc11     = 0
 vmr_cfc12     = 0
/
&nonhydrostatic_nml
 iadv_rhotheta  = 2
 ivctype        = 2
 itime_scheme   = 4
 exner_expol    = 0.333
 vwind_offctr   = 0.2
 damp_height    = 22500.
 rayleigh_coeff = 0.10   ! R3B7: 1.0 for forecasts, 5.0 in assimilation cycle; 0.5/2.5 for R2B6 (i.e. ensemble) runs
 lhdiff_rcf     = .true.
 divdamp_order  = 24    ! setting for forecast runs; use '2' for assimilation cycle
 divdamp_type   = 32    ! ** new setting for assimilation and forecast runs **
 divdamp_fac    = 0.004 ! use 0.032 in conjunction with divdamp_order = 2 in assimilation cycle
 l_open_ubc     = .false.
 igradp_method  = 3
 l_zdiffu_t     = .true.
 thslp_zdiffu   = 0.02
 thhgtd_zdiffu  = 125.
 htop_moist_proc= 22500.
 hbot_qvsubstep = 22500. ! use 22500. with R2B6
/
&nudging_nml
 nudge_type  = 0
 max_nudge_coeff_vn = 0.00000
/
&sleve_nml
 min_lay_thckn   = 20.
 max_lay_thckn   = 400.   ! maximum layer thickness below htop_thcknlimit
 htop_thcknlimit = 14000. ! this implies that the upcoming COSMO-EU nest will have 60 levels
 top_height      = 30000.
 stretch_fac     = 0.9
 decay_scale_1   = 4000.
 decay_scale_2   = 2500.
 decay_exp       = 1.2
 flat_height     = 16000.
/
&dynamics_nml
 iequations     = 3
 idiv_method    = 1
 divavg_cntrwgt = 0.50
 !lcoriolis      = .TRUE.
/
&transport_nml
 ctracer_list  = '12345'
 ivadv_tracer  = 3,3,3,3,3
 itype_hlimit  = 3,4,4,4,4
 ihadv_tracer  = 52,2,2,2,2
 iadv_tke      = 0
/
&diffusion_nml
 hdiff_order      = 5
 itype_vn_diffu   = 1
 itype_t_diffu    = 2
 hdiff_efdt_ratio = 24.0
 hdiff_smag_fac   = 0.025
 lhdiff_vn        = .TRUE.
 lhdiff_temp      = .TRUE.
/
&interpol_nml
 nudge_zone_width  = 0.0  ! deactivating nudging as needed for mass conservation for this setup 
 nudge_efold_width = 0.0
 nudge_max_coeff   = 0.00000
 lsq_high_ord      = 3
 l_intp_c2l        = .true.
 l_mono_c2l        = .true.
 support_baryctr_intp = .false.
 rbf_vec_scale_c              = 0.3,  0.1,  0.03
 rbf_vec_scale_v              = 0.4,  0.25, 0.07
 rbf_vec_scale_e              = 0.45, 0.37, 0.25
/
&extpar_nml
 itopo          = 1
 extpar_filename             = 'extpar_DOM01.nc'
 n_iter_smooth_topo = 1         
 heightdiff_threshold = 3000.
/
&io_nml
 itype_pres_msl = 5  ! 4 New extrapolation method to circumvent Ninjo problem with surface inversions
 itype_rh       = 1  ! RH w.r.t. water
 restart_file_type = 5       ! 4: netcdf2, 5: netcdf4
/
&nh_pzlev_nml
 nplev             = 18     ! number of p level output (attention: levels in [Pa] and top-down)
 nzlev             = 20     ! number of z level output (attention: levels in [m] and top-down)
 plevels           = 5000,7000,10000,15000,20000,25000,30000,40000,50000,60000,70000,
                     80000,85000,90000,92500,95000,97500,100000
 zlevels           = 25000,20000,18000,16000,14000,12000,10000,8000,6000,5000,4000,3000,2000,
                     1000,800,600,400,200,100,10
/
&output_nml
 filetype                     =  5                      ! output format: 2=GRIB2, 4=NETCDFv2
 dom                          =  1         
 output_start     = "${start_date}"
 output_end       = "${end_date}"
 output_interval  = "${output_interval}"
 file_interval    = "${file_interval}"
 include_last                 =  .FALSE.
 output_filename              = 'icon-cld3d' ! file name base
 filename_format  = "<output_filename>_ML_<datetime2>"
 ml_varlist                   = 'clc','tot_qv_dia','tot_qc_dia','tot_qi_dia'
 remap            = 0
 !reg_lon_def                  = ${reg_lon_def_reg}              !-180.,1,179.5
 !reg_lat_def                  = ${reg_lat_def_reg}              !89.5,-1, -89.5
/
!&output_nml
 !filetype                     =  5                      ! output format: 2=GRIB2, 4=NETCDFv2
 !dom                          =  1                      ! write all domains
 !output_start     = "${start_date}"
 !output_end       = "${end_date}"
 !output_interval  = "${output_interval}"
 !file_interval    = "${file_interval}"
 !include_last                 =  .FALSE.
 !output_filename              = 'icon-rad3d' ! file name base
 !filename_format  = "<output_filename>_ML_<datetime2>"
 !ml_varlist                   = 'lwflxall', 'lwflxclr','swflxall','swflxclr'
 !remap                        = 0
 !reg_lon_def                  = ${reg_lon_def_reg}
 !reg_lat_def                  = ${reg_lat_def_reg}
!/
&output_nml
 filetype                     =  5                      ! output format: 2=GRIB2, 4=NETCDFv2
 dom                          =  1                      ! write all domains
 output_start     = "${start_date}"
 output_end       = "${end_date}"
 output_interval  = "${output_interval}"
 file_interval    = "${file_interval}"
 include_last                 =  .FALSE.
 output_filename              = 'icon-atm3d' ! file name base
 filename_format  = "<output_filename>_ML_<datetime2>"
 ml_varlist                   = 'u','v','pv','temp','pres','pres_sfc','rho','geopot','omega','w','ddt_tke','z_ifc'
 remap                        = 0
 !reg_lon_def                  = ${reg_lon_def_reg}
 !reg_lat_def                  = ${reg_lat_def_reg}
/
!&output_nml
 !filetype                     =  5              ! output format: 2=GRIB2, 4=NETCDFv2
 !dom                          =  1                       ! write all domains
 !output_start     = "${start_date}"
 !output_end       = "${end_date}"
 !output_interval  = "${output_interval}"
 !file_interval    = "${file_interval}"
 !include_last                 = .FALSE.
 !output_filename              = 'icon-rad2d'   ! file name base
 !filename_format  = "<output_filename>_ML_<datetime2>"
 !ml_varlist                   = 'sob_t','thb_t','sobclr_t','thbclr_t','sob_s','thb_s','thbclr_s','sobclr_s'
 !remap                        = 0
 !reg_lon_def                  = ${reg_lon_def_reg}
 !reg_lat_def                  = ${reg_lat_def_reg}
!/
&output_nml
 filetype                     =  5                      ! output format: 2=GRIB2, 4=NETCDFv2
 dom                          =  1                      ! write all domains
 output_start     = "${start_date}"
 output_end       = "${end_date}"
 output_interval  = "${output_interval}"
 file_interval    = "${file_interval}"
 include_last                 =  .FALSE.
 output_filename              = 'icon-ddt_temp' ! file name base
 filename_format  = "<output_filename>_ML_<datetime2>"
 ml_varlist                   = 'ddt_temp_radswnw','ddt_temp_radlwnw','ddt_temp_radswcs','ddt_temp_radlwcs','ddt_temp_turb','ddt_temp_pconv','ddt_temp_dyn','ddt_temp_gscp','ddt_temp_mphy','ddt_temp_diff','ddt_temp_drag'
 remap                        = 0
 !reg_lon_def                  = ${reg_lon_def_reg}
 !reg_lat_def                  = ${reg_lat_def_reg}
/
&output_nml
 filetype                     =  5                      ! output format: 2=GRIB2, 4=NETCDFv2
 dom                          =  1                      ! write all domains
 output_start     = "${start_date}"
 output_end       = "${end_date}"
 output_interval  = "${output_interval}"
 file_interval    = "${file_interval}"
 include_last                 =  .FALSE.
 output_filename              = 'icon-ddt_wind' ! file name base
 filename_format  = "<output_filename>_ML_<datetime2>"
 ml_varlist                   = 'ddt_u_gwd','ddt_u_pconv','ddt_u_sso','ddt_u_turb','ddt_v_gwd','ddt_v_pconv','ddt_v_sso','ddt_v_turb','ddt_temp_dyn2'
 remap                        = 0
 !reg_lon_def                  = ${reg_lon_def_reg}
 !reg_lat_def                  = ${reg_lat_def_reg}
/
&output_nml
 filetype                     =  5              ! output format: 2=GRIB2, 4=NETCDFv2
 dom                          =  1                       ! write all domains
 output_start     = "${start_date}"
 output_end       = "${end_date}"
 output_interval  = "${output_interval}"
 file_interval    = "${file_interval}"
 include_last                 = .FALSE.
 output_filename              = 'icon-atm2d'   ! file name base
 filename_format  = "<output_filename>_ML_<datetime2>"
 ml_varlist                   = 'pres_msl','pres_sfc','group:precip_vars','rain_gsp_rate','rain_con_rate','cape_ml','cin_ml','clct','clch','clcm','clcl','tqv_dia','tqc_dia','tqi_dia','shfl_s','lhfl_s','w_so'
 remap                        = 0
 !reg_lon_def                  = ${reg_lon_def_reg}
 !reg_lat_def                  = ${reg_lat_def_reg}
/
&output_nml
 output_filename  = "icon-extra"
 filename_format  = "<output_filename>_<levtype_l>"
 filetype         = 5
 remap            = 0
 output_grid      = .TRUE.
 output_start     = "${start_date}"          ! output_start = output_end
 output_end       = "${start_date}"          ! --> write once only irrespective of
 output_interval  = "PT1H"  !     the output interval and
 file_interval    = "${file_interval}"    !     the file interval
 ml_varlist       = 'z_mc'
/
EOF

#=============================================================================
#
# This section of the run script prepares and starts the model integration. 
#-----------------------------------------------------------------------------

final_status_file=${EXPDIR}/${job_name}.final_status #!NICOLE
rm -f ${final_status_file}
#-----------------------------------------------------------------------------

# set up the model lists if they do not exist
# this works for single model runs
# for coupled runs the lists should be declared explicilty
if [ x$namelist_list = x ]; then
#  minrank_list=(        0           )
#  maxrank_list=(     65535          )
#  incrank_list=(        1           )
  minrank_list[0]=0
  maxrank_list[0]=65535
  incrank_list[0]=1
  if [ x$atmo_namelist != x ]; then
    # this is the atmo model
    namelist_list[0]="$atmo_namelist"
    modelname_list[0]="atmo"
    modeltype_list[0]=1
    run_atmo="true"
  else
    check_error 1 "No namelist is defined"
  fi
fi

#-----------------------------------------------------------------------------
# set some default values and derive some run parameteres
restart=${restart:=".false."}
restartSemaphoreFilename='isRestartRun.sem'
#AUTOMATIC_RESTART_SETUP:
if [ -f ${restartSemaphoreFilename} ]; then
  restart=.true.
  #  do not delete switch-file, to enable restart after unintended abort
  #[[ -f ${restartSemaphoreFilename} ]] && rm ${restartSemaphoreFilename}
fi
#END AUTOMATIC_RESTART_SETUP
#
# wait 5min to let GPFS finish the write operations
if [ "x$restart" != 'x.false.' -a "x$submit" != 'x' ]; then
  if [ x$(df -T ${EXPDIR} | cut -d ' ' -f 2) = gpfs ]; then
    sleep 10;
  fi
fi
# fill some checks

run_atmo=${run_atmo="false"}
if [ x$atmo_namelist != x ]; then
  run_atmo="true"
fi
#-----------------------------------------------------------------------------
# get restart files

if  [ x$restart_atmo_from != "x" ] ; then
  rm -f restart_atm_DOM01.nc
#  ln -s ${basedir}/experiments/${restart_from_folder}/${restart_atmo_from} ${EXPDIR}/restart_atm_DOM01.nc
  cp ${basedir}/experiments/${restart_from_folder}/${restart_atmo_from} cp_restart_atm.nc
  ln -s cp_restart_atm.nc restart_atm_DOM01.nc
  restart=".true."
fi
if  [ x$restart_ocean_from != "x" ] ; then
  rm -f restart_oce.nc
#  ln -s ${basedir}/experiments/${restart_from_folder}/${restart_ocean_from} ${EXPDIR}/restart_oce.nc
  cp ${basedir}/experiments/${restart_from_folder}/${restart_ocean_from} cp_restart_oce_DOM01.nc
  ln -s cp_restart_oce_DOM01.nc restart_oce_DOM01.nc
  restart=".true."
fi
#-----------------------------------------------------------------------------


read_restart_namelists=${read_restart_namelists:=".true."}

#-----------------------------------------------------------------------------
#
# create ICON master namelist
# ------------------------
# For a complete list see Namelist_overview and Namelist_overview.pdf

#-----------------------------------------------------------------------------
# create master_namelist
master_namelist=icon_master.namelist
if [ x$end_date = x ]; then
cat > $master_namelist << EOF
&master_nml
 lrestart            = $restart
/
&master_time_control_nml
 experimentStartDate  = "$start_date"
 restartTimeIntval    = "$restart_interval"
 checkpointTimeIntval = "$checkpoint_interval"
/
&time_nml
 is_relative_time = .false.
/
EOF
else
if [ x$calendar = x ]; then
  calendar='proleptic gregorian'
  calendar_type=1
else
  calendar=$calendar
  calendar_type=$calendar_type
fi
cat > $master_namelist << EOF
&master_nml
 lrestart            = $restart
 read_restart_namelists = $read_restart_namelists
/
&master_time_control_nml
 calendar             = "$calendar"
 checkpointTimeIntval = "$checkpoint_interval"
 restartTimeIntval    = "$restart_interval"
 experimentStartDate  = "$start_date"
 experimentStopDate   = "$end_date"
/
&time_nml
 is_relative_time = .false.
/
EOF
fi
#-----------------------------------------------------------------------------


#-----------------------------------------------------------------------------
# add model component to master_namelist
add_component_to_master_namelist()
{

  model_namelist_filename="$1"
  model_name=$2
  model_type=$3
  model_min_rank=$4
  model_max_rank=$5
  model_inc_rank=$6

cat >> $master_namelist << EOF
&master_model_nml
  model_name="$model_name"
  model_namelist_filename="$model_namelist_filename"
  model_type=$model_type
  model_min_rank=$model_min_rank
  model_max_rank=$model_max_rank
  model_inc_rank=$model_inc_rank
/
EOF

}
#-----------------------------------------------------------------------------


no_of_models=${#namelist_list[*]}
echo "no_of_models=$no_of_models"

j=0
while [ $j -lt ${no_of_models} ]
do
  add_component_to_master_namelist "${namelist_list[$j]}" "${modelname_list[$j]}" ${modeltype_list[$j]} ${minrank_list[$j]} ${maxrank_list[$j]} ${incrank_list[$j]}
  j=`expr ${j} + 1`
done

#-----------------------------------------------------------------------------
#
# get model
#
ls -l ${MODEL}
check_error $? "${MODEL} does not exist?"
#-----------------------------------------------------------------------------

# start experiment

rm -f finish.status
date
${START} ${MODEL}
date
#
if [ -r finish.status ] ; then
  check_final_status 0 "${START} ${MODEL}"
else
  check_final_status -1 "${START} ${MODEL}"
fi
#

#-----------------------------------------------------------------------------
#
finish_status=`cat finish.status`
echo $finish_status
echo "============================"
echo "Script run successfully: $finish_status"
echo "============================"
#-----------------------------------------------------------------------------
namelist_list=""
#-----------------------------------------------------------------------------
# check if we have to restart, ie resubmit
#   Note: this is a different mechanism from checking the restart
if [ $finish_status = "RESTART" ]; then
  echo "restart next experiment..."
  this_script="${RUNSCRIPTDIR}/exp.${EXPNAME}.run"
  echo 'this_script: ' "$this_script"
  # note that if ${restartSemaphoreFilename} does not exist yet, then touch will create it
  touch ${restartSemaphoreFilename}
  cd ${RUNSCRIPTDIR}
  sbatch exp.${EXPNAME}.run
else
  [[ -f ${restartSemaphoreFilename} ]] && rm ${restartSemaphoreFilename}
fi
#-----------------------------------------------------------------------------

#-----------------------------------------------------------------------------

cd ${RUNSCRIPTDIR}

#-----------------------------------------------------------------------------
